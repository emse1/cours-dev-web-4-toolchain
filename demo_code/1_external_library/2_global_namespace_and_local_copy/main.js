/* 
  axios exists in the global namespace,
  but we need to trust the HTML document to have imported the axios library before 
  this script

*/


axios.get('https://pokeapi.co/api/v2/pokemon/1')
  .then(function (response) {
    // handle success
    console.log(response.data.name);
  });