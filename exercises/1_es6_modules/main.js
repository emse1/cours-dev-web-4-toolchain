function fetchPokemonAndDisplayIt(pokemonId) {
  fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonId}`)
  .then(response => response.json())
  .then(response => {
    // Display the name of the pokemon on the console
    let pokemonName = response.name;
    console.log('Pokemon name : ' + pokemonName);
    // Add a <p> element with the pokemon name to the DOM tree
    insertParagraph(pokemonName)
  })
}


function insertParagraph(textContent) {
  let newParagraph = document.createElement("p");
  newParagraph.textContent = textContent;
  document.querySelector(".main-content").appendChild(newParagraph);
}




fetchPokemonAndDisplayIt(5);