The goal of the exercise is to refactor the given JS source code into separate modules.

The functions `insertParagraph()` and `fetchPokemonAndDisplayIt()` should be in 2 seperate source files.

Use the ES6 module system (with keywords `ìmport` and `export`) in order to link your source files.

The `main.html` document should have only one `<script>` tag linking to your code entrypoint `main.js`. Do not use ES5 mechanism of global namespace sharing between scripts.

When accessing the webpage `main.html` through the server running at `localhost:8000` (script `./run.sh` at the root of the repository), you should see the name of the pokemon of the ID you gave in `main.js`.