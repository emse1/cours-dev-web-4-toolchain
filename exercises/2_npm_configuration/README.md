In this directory you will find a small frontend project :

- There is a small nodeJS server dedicated to serve the frontend (HTML + all assets including frontend JS code). The server
  code is in `./server` 
- The frontend is in the `./dist` server.

The goal of this exercise is to finalize the NPM configuration, in order to have the nodeJS server working.

# Step 1

Modify the `package.json` file in order to setup the JS dependencies needed for the server code to run (remember, the server code is `./server/index.js`).

You can use `npm install <package_name>` in order to automatically add a new dependency to the `package.json` file, or you can edit the file manually and then run `npm install` to install all the dependencies specified in the config file.

# Step 2

Add a script to the `package.json` in order to be able to start the web server with the command `npm run start`.
If your server runs correctly, you should be able to access `http://localhost:3000`