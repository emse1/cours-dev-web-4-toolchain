The goal of this exercise is to add SCSS support to our webpack configuration. 

The `main.js` file imports the `style.scss` source file, but for now our webpack configuration doesn't know how to handle this file, resulting in an error on build.

According to this documentation : https://webpack.js.org/loaders/sass-loader/

We need to add the following NPM dependencies : `sass` and `sass-loader`.

In the `webpack.config.js`, you need to add a new module rule :
- the rule should match files ending in `.scss`
- the rule should apply the following chain of loaders : `sass-loader`, then `css-loader` and finally `style-loader`. 
  Be careful about the webpack convention for loader order. 

When running `npm run build`, the build should run without errors (warnings are expected), and on `npm run start`, you should be able to access `localhost:3000` with the correct styles applied.  Thanks to the source map configuration, in your chrome developper tools, you will be able to inspect your styles, linked directly to your SCSS source, although the browser is fed with the CSS transpiled from your SCSS sources.